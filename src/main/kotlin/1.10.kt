/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Quina és la mida de la meva pizza?
*/
import java.util.*

fun superficie (diametre : Double): Double{
    return Math.PI*(diametre/2)*(diametre/2)
}
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el dièmetre de la pizza en cm:")
    val diametre= scanner.nextDouble()


    println("Aquesta és la superfície de la pizza: ${superficie(diametre)} cm quadrats ")


}
/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Calcula l’àrea
*/
import java.util.*

fun area (amplada :Int, llargada:Int) :Long {
    return if (amplada < 0 || llargada < 0) {
        0
    } else {
        amplada * llargada.toLong()
    }
}



fun main() {
    val scanner = Scanner(System.`in`)
    println("Amplada:")
    val amplada = scanner.nextInt()
    println("Llargada:")
    val llargada  = scanner.nextInt()

        println("Àrea: ${area(amplada,llargada)} metres quadrats")

}



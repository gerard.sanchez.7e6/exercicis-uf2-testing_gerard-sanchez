/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Suma de dos nombres enters
*/
import java.util.*

fun sum (primernumero : Int, segonnumero : Int):Long{
    return (primernumero + segonnumero).toLong()
}
fun main() {
    val scannner = Scanner(System.`in`)
    println("Introdueix un número:")
    val primernumero = scannner.nextInt()

    val segonscanner = Scanner(System.`in`)
    println("Introdueix un segon número:")
    val segonnumero = segonscanner.nextInt()

    println("Resultat: ${sum(primernumero,segonnumero)}")

}







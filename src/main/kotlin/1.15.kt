/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/21
* TITLE: Afegeix un segon
 */
import java.util.*

fun addOneSecond ( segon: Int): Any {
    return if (segon !in 0 ..59){
        "No és possible"
    }
    else{
        return (segon+1)%60
    }
}
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un segon:")
    val segon = scanner.nextInt()

    println(addOneSecond(segon))

}
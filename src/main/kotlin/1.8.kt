/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Dobla el decimal
 */
import java.math.BigDecimal

import java.util.*

fun multiplicacio (userInputValue :Double): Double{
    return (userInputValue * 2)
}
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número decimal:")
    val userInputValue = scanner.nextDouble()

    println("Aquest és el número introduït: ${multiplicacio(userInputValue)}")

}


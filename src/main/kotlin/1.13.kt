/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Quina temperatura fa?
*/
import java.util.*


fun graus ( graus: Double, augmentgraus: Double): Double{

    return graus + augmentgraus
}

fun main() {

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Temperatura:")
    val graus = scanner.nextDouble()

    val segonscanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Augment de temperatura:")
    val augmentgraus = segonscanner.nextDouble()

    print("La temperatura actual és de ${graus( graus, augmentgraus)} º")


}
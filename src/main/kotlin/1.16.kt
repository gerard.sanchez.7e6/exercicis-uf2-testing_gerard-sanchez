/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/21
* TITLE: Transforma l’enter
 */
import java.util.*

fun transformToDouble ( numero: Int): Double{

    return numero.toDouble()
}
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un nombre enter:")
    val numero = scanner.nextInt()
    println("El seu número decimal: ${transformToDouble(numero)} ")

}
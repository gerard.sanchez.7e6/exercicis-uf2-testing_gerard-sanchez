/*
* AUTHOR: Gerard SAnchez Preto
* DATE: 22/09/19
* TITLE: Calculadora de volum d’aire
*/
import java.util.*

fun volum (llargada : Double,amplada : Double,alcada: Double): Double{
    return if (llargada < 0.0 || amplada < 0.0 || alcada < 0.0){
        0.0
    }
    else{
        return llargada * amplada * alcada
    }


}
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix la llargada de l'habitació:")
    val llargada = scanner.nextDouble()

    println("Introdueix l'amplada de l'habitació:")
    val amplada = scanner.nextDouble()


    println("Introdueix l'alçada de l'habitació:")
    val alcada = scanner.nextDouble()

    println("Aquest és el volum d'aire de l'habitació: ${volum(alcada,llargada,amplada)} metres cubics")

}
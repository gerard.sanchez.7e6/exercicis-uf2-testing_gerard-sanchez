/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Pupitres
*/
import java.util.*

fun pupitres (primeraclasse :Int, segonclasse: Int, terceraclasse: Int): Int{
    return (primeraclasse + segonclasse + terceraclasse + 1) / 2
}
fun main() {
    val scanner = Scanner(System.`in`)
    println("Alumnes primera classe:")
    val primeraclasse = scanner.nextInt()

    println("Alumnes segona classe:")
    val segonaclasse = scanner.nextInt()

    println("Alumnes tercera classe")
    val terceraclasse = scanner.nextInt()


    println("Pupitres necessaris: ${pupitres(primeraclasse,segonaclasse,terceraclasse)}")

}
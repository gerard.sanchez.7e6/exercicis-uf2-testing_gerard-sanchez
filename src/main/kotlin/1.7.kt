/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Número següent
*/
import java.util.*

fun sum (userInputValue: Int):Long{
    return (userInputValue + 1).toLong()
}
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt()

    print("Després ve el ${sum(userInputValue)} ")


}
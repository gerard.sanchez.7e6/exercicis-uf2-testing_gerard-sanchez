/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Operació boja
*/
import java.util.*

fun operation (primernumero : Int, segonnumero : Int, tercernumero :Int, quartnumero :Int): Long{
    return if ( quartnumero == 0){
        0
    }else{
        return ((primernumero + segonnumero) * (tercernumero % quartnumero)).toLong()
    }

}
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val primernumero = scanner.nextInt()

    println("Introdueix un segon número:")
    val segonnumero = scanner.nextInt()

    println("Introdueix un tercer número:")
    val tercernumero = scanner.nextInt()

    println("Introdueix un quart número:")
    val quartnumero = scanner.nextInt()

    println("Resultat: ${operation(primernumero,segonnumero,tercernumero,quartnumero)}")

}

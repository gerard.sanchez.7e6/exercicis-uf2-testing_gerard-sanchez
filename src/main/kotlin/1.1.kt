/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Hello World!
*/

fun main() {
    println("Hello World")
}

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: De Celsius a Fahrenheit
*/
import java.util.*

fun fahrenheit ( graus: Double): Double{

    return (graus*1.8)+32
}
fun main() {
    println("Introdueix la temperatura en graus Celsius:")
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val graus= scanner.nextDouble()

    println("Aquesta és la temperatura en graus Fahrenheit: ${fahrenheit(graus)} ºF")


}
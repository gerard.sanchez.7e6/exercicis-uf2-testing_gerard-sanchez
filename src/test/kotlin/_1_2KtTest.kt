import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_2KtTest {

    @Test
    fun checkNumberIsLongNumber() {
        assertEquals(1000000000, multiplicacio(500000000))
    }
    @Test
    fun checkNegativeNumberIsNegativeLongNumber() {
        assertEquals(-1000000000, multiplicacio(-500000000))
    }
    @Test
    fun check0NumberIs0() {
        assertEquals(0, multiplicacio(0))
    }
}
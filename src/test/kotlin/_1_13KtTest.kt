import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{
    @Test
    fun checkResultIfTemperatureIsNegative(){
        assertEquals(-9.399999999999999 , graus(-21.9,12.5))
    }
    @Test
    fun checkResultWithPositiveNumbers(){
        assertEquals(25.5, graus(21.6,3.9))
    }
    @Test
    fun checkResultIfTemperatureRiseIsNegative(){
        assertEquals(20.1 , graus(24.8, -4.7))
    }

}


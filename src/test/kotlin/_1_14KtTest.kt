import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_14KtTest{
    @Test
    fun checkResultWithPositiveNumbers(){
        assertEquals(20.89, priceForPerson(5,104.45))
    }
    @Test
    fun checkResultIfPersonIs0(){
        assertEquals(0.0, priceForPerson(0,309.98))
    }
    @Test
    fun checkResultIfPriceIs0(){
        assertEquals(0.0, priceForPerson(4,0.0))
    }

}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_12KtTest{
    @Test
    fun checkResultWithPositiveNumber(){
        assertEquals(44.42, fahrenheit(6.9))
    }
    @Test
    fun checkResultWith0(){
        assertEquals(32.0 , fahrenheit(0.0))
    }
    @Test
    fun checkResultWithNegativeNumber(){
        assertEquals(10.940000000000001, fahrenheit(-11.7))
    }
}


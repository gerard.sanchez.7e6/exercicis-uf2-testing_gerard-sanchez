import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_16KtTest{
    @Test
    fun checkResultIfNumberIs0(){
        assertEquals(0.0, transformToDouble(0))
    }
    @Test
    fun checkResultIfNumberIsPositive(){
        assertEquals(34.0, transformToDouble(34))
    }
    @Test
    fun checkResultIfNumberIsNegative(){
        assertEquals(-567.0, transformToDouble(-567))
    }

}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3KtTest{
    @Test
    fun checkNumberIsLongNumber() {
        assertEquals(1000000000, sum(500000000, 500000000 ))
    }
    @Test
    fun checkNegativeNumberIsNegativeLongNumber() {
        assertEquals(-1000000000, sum(-500000000,-500000000))
    }
    @Test
    fun checkNegativeNumberWithPositiveNumber() {
        assertEquals(0, sum(-123,123))
    }
}

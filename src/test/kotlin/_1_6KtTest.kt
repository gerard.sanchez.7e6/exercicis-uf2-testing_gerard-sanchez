import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{
    @Test
    fun checkIfResultIsTrue() {
        assertEquals(32, pupitres(23,34 , 7, ))
    }
    @Test
    fun checkResultIfPupitresArePair() {
        assertEquals(9, pupitres(4,6, 8))
    }
    @Test
    fun checkResultIfPupitresAreOdd () {
        assertEquals(18, pupitres(23,7, 5))
    }
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_8KtTest{
    @Test
    fun checkResultIfNumberIsWholeNumber(){
        assertEquals(1174.0, multiplicacio(587.0))
    }
    @Test
    fun checkResultWithShortNumber(){
        assertEquals(4.68, multiplicacio(2.34))
    }
    @Test
    fun checkResultWithNegativeNumber(){
        assertEquals(-43.08, multiplicacio(-21.54))
    }


}
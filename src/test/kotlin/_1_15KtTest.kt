import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_15KtTest{
    @Test
    fun checkResultIfSecondIs0(){
        assertEquals(1, addOneSecond(0))
    }
    @Test
    fun checkResultIfSecondIs59(){
        assertEquals(0, addOneSecond(59))
    }
    @Test
    fun checkResultIfSecondIs78(){
        assertEquals("No és possible", addOneSecond(78))
    }

}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_5KtTest{
    @Test
    fun checkResultIfFourthNumberIs0() {
        assertEquals(0, operation(56, 4, 7, 0  ))
    }
    @Test
    fun checkResultIfNumbersArePositive() {
        assertEquals(48, operation(3,5, 6,8))
    }
    @Test
    fun checkIfResultIsLongNumber() {
        assertEquals(1150000000, operation(250000000,900000000, 9, 2))
    }
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_9KtTest{
    @Test
    fun checkIsSecondPriceIs0(){
        assertEquals(100.0, descompte(100.0,0.0))
    }
    @Test
    fun checkIsSecondPriceAndFirstPriceAre0(){
        assertEquals(0.0, descompte(0.0,0.0))
    }
    @Test
    fun checkResutlIsCorrect(){
        assertEquals(23.161845882093175 , descompte(135.87,104.4))
    }


}
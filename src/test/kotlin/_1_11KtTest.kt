import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_11KtTest{
    @Test
    fun checkResultIfNumbersArePositive(){
        assertEquals(3689.3880000000004, volum(23.6,57.9,2.7))
    }
    @Test
    fun checkResultIfNumberIsNegative(){
        assertEquals(0.0, volum(-2.3,4.7,6.9))
    }
    @Test
    fun checkResultIfNumberIs0(){
        assertEquals(0.0, volum(5.7,0.0,8.9))
    }
}


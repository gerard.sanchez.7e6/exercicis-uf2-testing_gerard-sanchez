import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{
    @Test
    fun checkIfDiametreIs0(){
        assertEquals(0.0,superficie(0.0))
    }
    @Test
    fun checkIfDiametreIsSimpleNumber(){
        assertEquals(415.4756284372501,superficie(23.0))
    }
    @Test
    fun checkIfDiametreIsBigNumber(){
        assertEquals(159517.0757457475 ,superficie(450.67))
    }
}
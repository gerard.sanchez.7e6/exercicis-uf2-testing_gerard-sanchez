import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{
    @Test
    fun checkIfResultIsLongNumber(){
        assertEquals(1000000000, sum(999999999))
    }
    @Test
    fun checkIfResultAreCorrect(){
        assertEquals(36, sum(35))
    }
    @Test
    fun checkResultIfFirstNegativeNumberIs0(){
        assertEquals(0, sum(-1))
    }

}
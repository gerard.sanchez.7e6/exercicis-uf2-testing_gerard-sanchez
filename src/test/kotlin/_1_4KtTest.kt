import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{
    @Test
    fun checkNumberIsLongNumber() {
        assertEquals(1500000000, area(500000000, 3 ))
    }
    @Test
    fun checkNegativeNumberIsNegativeNumber() {
        assertEquals(0, area(-3,-5))
    }
    @Test
    fun checkNegativeNumberWithPositiveNumber() {
        assertEquals(0, area(-123,123))
    }
}

